## ui.R controls the layout and appearance of the app
## remember that each ui function needs to be coonected with the server
myPal = c(brewer.pal(10,'Set3'),brewer.pal(12,"Paired"),brewer.pal(3,"Accent")) ## one color per species


ui <- fluidPage(
    
    tags$head(tags$style(".rightAlign{float:right;}"),
              tags$style("label {font-size:12px;}")
    ),
    
    titlePanel(h1("Overview species distribution",align='center')),
    
    ## this adds a widget that allows user to select the input file
    fileInput(
        inputId = 'inputFile', 'Select the input file (.csv)',
        accept = c('text/csv','text/comma-separated-values,text/plain','.csv')
        ),

    sidebarLayout(

        sidebarPanel(
            
            ## widgets for 1st plot
            ## one color for each species
           numericRangeInput(
            inputId = "lat", label = "Latitude:",
            value = c(-90,90) ## default values 
          ),
          numericRangeInput(
            inputId = "long", label = "Longitude:",
            value = c(-180, 180) ## default values 
          ),
          downloadButton("downloadPlot_all", "Download the 1st figure"),
          
          fluidRow(
            column(12,h5("Species 1")),
            column(4,
                   colourInput("col_species1", "Fill", myPal[1]),
                   
            ),
            column(4,
                   colourInput("point_border1", "Border", "black"),
            ),
            column(4,
                   selectInput(
                     "point_shape1",
                     label = "Shape",
                     choices = c(
                       'Circles' = 21,
                       'Triangles' = 24,
                       'Squares' = 22,
                       'Diamonds' = 23,
                       'Inv Triangles' = 25
                     )
                   ),
                   ),
            column(6,
                   sliderInput(
              "point_size1", label = "Size",
              min = 0, value = 2, max = 10),
            ),
            
            column(6,
                   sliderInput(
              "point_alpha1", label = "Transparency",
              min = 0, value = 0.5, max = 1),
            ),
            
            ),
          fluidRow(
            column(12,h5("Species 2")),
            column(4,
                   colourInput("col_species2", "Fill", myPal[2]),
                   
            ),
            column(4,
                   colourInput("point_border2", "Border", "black"),
            ),
            column(4,
                   selectInput(
                     "point_shape2",
                     label = "Shape",
                     choices = c(
                       'Circles' = 21,
                       'Triangles' = 24,
                       'Squares' = 22,
                       'Diamonds' = 23,
                       'Inv Triangles' = 25
                     )
                   ),
            ),
            column(6,
                   sliderInput(
                     "point_size2", label = "Size",
                     min = 0, value = 2, max = 10),
            ),
            
            column(6,
                   sliderInput(
                     "point_alpha2", label = "Transparency",
                     min = 0, value = 0.5, max = 1),
            ),
            
          ),
          fluidRow(
            column(12, h5("Species 3")),
            column(4, colourInput("col_species3", "Fill", myPal[3])),
            column(4, colourInput("point_border3", "Border", "black")),
            column(4, selectInput(
              "point_shape3",
              label = "Shape",
              choices = c(
                'Circles' = 21,
                'Triangles' = 24,
                'Squares' = 22,
                'Diamonds' = 23,
                'Inv Triangles' = 25
              )
            )),
            column(6, sliderInput("point_size3", label = "Size", min = 0, value = 2, max = 10)),
            column(6, sliderInput("point_alpha3", label = "Transparency", min = 0, value = 0.5, max = 1)),
          ),
          fluidRow(
            column(12, h5("Species 4")),
            column(4, colourInput("col_species4", "Fill", myPal[4])),
            column(4, colourInput("point_border4", "Border", "black")),
            column(4, selectInput(
              "point_shape4",
              label = "Shape",
              choices = c(
                'Circles' = 21,
                'Triangles' = 24,
                'Squares' = 22,
                'Diamonds' = 23,
                'Inv Triangles' = 25
              )
            )),
            column(6, sliderInput("point_size4", label = "Size", min = 0, value = 2, max = 10)),
            column(6,  
                    sliderInput(
                            "point_alpha4", label = "Transparency",
                            min = 0, value = 0.5, max = 1),
                   ),
                   
            ),
          fluidRow(
            column(12,h5("Species 5")),
            column(4,
                   colourInput("col_species5", "Fill", myPal[5]),
                   
            ),
            column(4,
                   colourInput("point_border5", "Border", "black"),
            ),
            column(4,
                   selectInput(
                     "point_shape5",
                     label = "Shape",
                     choices = c(
                       'Circles' = 21,
                       'Triangles' = 24,
                       'Squares' = 22,
                       'Diamonds' = 23,
                       'Inv Triangles' = 25
                     )
                   ),
            ),
            column(6,
                   sliderInput(
                     "point_size5", label = "Size",
                     min = 0, value = 2, max = 10),
            ),
            
            column(6,
                   sliderInput(
                     "point_alpha5", label = "Transparency",
                     min = 0, value = 0.5, max = 1),
            ),
            
          ),
          fluidRow(
            column(12,h5("Species 6")),
            column(4,
                   colourInput("col_species6", "Fill", myPal[6]),
                   
            ),
            column(4,
                   colourInput("point_border6", "Border", "black"),
            ),
            column(4,
                   selectInput(
                     "point_shape6",
                     label = "Shape",
                     choices = c(
                       'Circles' = 21,
                       'Triangles' = 24,
                       'Squares' = 22,
                       'Diamonds' = 23,
                       'Inv Triangles' = 25
                     )
                   ),
            ),
            column(6,
                   sliderInput(
                     "point_size6", label = "Size",
                     min = 0, value = 2, max = 10),
            ),
            
            column(6,
                   sliderInput(
                     "point_alpha6", label = "Transparency",
                     min = 0, value = 0.5, max = 1),
            ),
            
          ),
          fluidRow(
            column(12,h5("Species 7")),
            column(4,
                   colourInput("col_species7", "Fill", myPal[7]),
                   
            ),
            column(4,
                   colourInput("point_border7", "Border", "black"),
            ),
            column(4,
                   selectInput(
                     "point_shape7",
                     label = "Shape",
                     choices = c(
                       'Circles' = 21,
                       'Triangles' = 24,
                       'Squares' = 22,
                       'Diamonds' = 23,
                       'Inv Triangles' = 25
                     )
                   ),
            ),
            column(6,
                   sliderInput(
                     "point_size7", label = "Size",
                     min = 0, value = 2, max = 10),
            ),
            
            column(6,
                   sliderInput(
                     "point_alpha7", label = "Transparency",
                     min = 0, value = 0.5, max = 1),
            ),
            
          ),
          
          fluidRow(
            column(12,h5("Species 8")),
            column(4,
                   colourInput("col_species8", "Fill", myPal[8]),
                   
            ),
            column(4,
                   colourInput("point_border8", "Border", "black"),
            ),
            column(4,
                   selectInput(
                     "point_shape8",
                     label = "Shape",
                     choices = c(
                       'Circles' = 21,
                       'Triangles' = 24,
                       'Squares' = 22,
                       'Diamonds' = 23,
                       'Inv Triangles' = 25
                     )
                   ),
            ),
            column(6,
                   sliderInput(
                     "point_size8", label = "Size",
                     min = 0, value = 2, max = 10),
            ),
            
            column(6,
                   sliderInput(
                     "point_alpha8", label = "Transparency",
                     min = 0, value = 0.5, max = 1),
            ),
            
          ),
          fluidRow(
            column(12,h5("Species 9")),
            column(4,
                   colourInput("col_species9", "Fill", myPal[9]),
                   
            ),
            column(4,
                   colourInput("point_border9", "Border", "black"),
            ),
            column(4,
                   selectInput(
                     "point_shape9",
                     label = "Shape",
                     choices = c(
                       'Circles' = 21,
                       'Triangles' = 24,
                       'Squares' = 22,
                       'Diamonds' = 23,
                       'Inv Triangles' = 25
                     )
                   ),
            ),
            column(6,
                   sliderInput(
                     "point_size9", label = "Size",
                     min = 0, value = 2, max = 10),
            ),
            
            column(6,
                   sliderInput(
                     "point_alpha9", label = "Transparency",
                     min = 0, value = 0.5, max = 1),
            ),
            
          ),
          fluidRow(
            column(12,h5("Species 10")),
            column(4,
                   colourInput("col_species10", "Fill", myPal[10]),
                   
            ),
            column(4,
                   colourInput("point_border10", "Border", "black"),
            ),
            column(4,
                   selectInput(
                     "point_shape10",
                     label = "Shape",
                     choices = c(
                       'Circles' = 21,
                       'Triangles' = 24,
                       'Squares' = 22,
                       'Diamonds' = 23,
                       'Inv Triangles' = 25
                     )
                   ),
            ),
            column(6,
                   sliderInput(
                     "point_size10", label = "Size",
                     min = 0, value = 2, max = 10),
            ),
            
            column(6,
                   sliderInput(
                     "point_alpha10", label = "Transparency",
                     min = 0, value = 0.5, max = 1),
            ),
            
          ),      
          

          
    
           
           
            downloadButton("downloadPlot_all2", "Download the 1st figure")
            
    ),

    mainPanel(
        column(10,plotOutput("plot_all"))
        )
    )
    
  


    
 
)






